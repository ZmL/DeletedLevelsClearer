import mysql.connector
import os
import requests
import re
import json

from dotenv import load_dotenv

load_dotenv()

# [(0) Название уровня, [1] Ник автора, (2) ID уровня, (3) Текущая оценка, (4) Наименование оценки, (5) Загрузки, (6) Лайки, (7) Длина, (8) Сонг]
def get_parsed_level_data(level_id: int) -> list:
    page = requests.get(f'https://gdbrowser.com/{level_id}')

    matches = re.findall(r'content="(.+?)"', str(page.content))
    print(matches)

    return True if matches[0] == 'Level Search' else False


db = mysql.connector.connect(
    host=os.getenv("DB_HOST"),
    port=os.getenv("DB_PORT"),
    database=os.getenv("DB_NAME"),
    user=os.getenv("DB_USER"),
    password=os.getenv("DB_PASSWORD")
)

cursor = db.cursor()

cursor.execute("SELECT level_id, sender_id FROM requests_table")
levels_to_delete = []
senders = []


for x in cursor:
    level = get_parsed_level_data(int(list(x)[0]))
    if level:
        print("ЗАШЁЛ В IF\n")
        levels_to_delete.append(str(list(x)[0]))
        senders.append(str(list(x)[1]))


with open("warnings.json", "r") as file:
    warnings = json.load(file)


for s in senders:
    try:
        warnings[s] = warnings[s]+1
    except Exception:
        warnings[s] = 1


with open("warnings.json", "w") as file:
    json.dump(warnings, file, indent=2)


cursor.execute(f"DELETE FROM requests_table WHERE level_id = {' OR level_id = '.join(levels_to_delete)}")
db.commit()
print(f"Были удалены следующие уровни: {', '.join(levels_to_delete)}\nАвторы удалённых уровней: {', '.join(senders)}")